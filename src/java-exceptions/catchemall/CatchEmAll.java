package java_exceptions.catchemall;

import java.io.FileNotFoundException;
import java.io.IOException;

public class CatchEmAll {

    //You may set another exception in this field;
    static Exception exception = new FileNotFoundException();

    public static void riskyMethod() throws Exception {
        try {
            throw exception;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Resource is missing", e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Resource error", e);
        }
    }

    public static void main(String[] args) throws Exception {
        try {
            riskyMethod();
            System.out.println(10 / 0);
        } catch (NumberFormatException | ArithmeticException e) {
            System.err.println(e.getMessage());
        }
    }
}
