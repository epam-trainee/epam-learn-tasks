package java_collections;

import java.util.*;

public class Words {

    public String countWords(List<String> lines) {
        Map<String, Integer> res = new HashMap<>();
        StringBuilder resultStr = new StringBuilder();

        for (String line : lines) {
            String[] arr = line.split("[^а-яА-Яa-zA-Z]");
            for (String str : arr) {
                str = str.toLowerCase();
                if (str.length() >= 4) {
                    if (res.containsKey(str)) {
                        res.put(str.toLowerCase(), res.get(str) + 1);
                    } else {
                        res.put(str.toLowerCase(), 1);
                    }
                }
            }
        }
        List<Map.Entry<String, Integer>> list = new ArrayList(res.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
                int comp = b.getValue().compareTo(a.getValue());
                if (comp==0) {
                    comp= a.getKey().compareTo(b.getKey());
                }
                return comp;
            }
        });
        for (Map.Entry<String, Integer> entr : list){
            if (entr.getValue() >= 10) {
                resultStr.append(entr.getKey() + " - " + entr.getValue() + "\n");
            }
        }
        return resultStr.toString().trim();
    }
}
