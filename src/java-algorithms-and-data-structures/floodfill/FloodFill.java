package floodfill;

import java.util.Arrays;

public interface FloodFill {
    void flood(final String map, final FloodLogger logger);

    static FloodFill getInstance() {
        // throw new UnsupportedOperationException();
        FloodFill floodFill = new FloodFill() {

            private char[][] toCharArray(String map) {
                String[] array = map.split("\n");
                int lengthColumn = array.length;
                int lengthRow = array[0].length();
                char[][] area = new char[lengthColumn][lengthRow];
                for (int i = 0; i < lengthColumn; i++) {
                    for (int j = 0; j < lengthRow; j++) {
                        String str = array[i];
                        area[i][j] = str.charAt(j);
                    }
                }
                return area;
            }

            @Override
            public void flood(String map, FloodLogger logger) {
                logger.log(map);
                char[][] area = toCharArray(map);
                char[][] resultStep = new char[area.length][area[0].length];
                for (int i = 0; i < area.length; i++) {
                    for (int j = 0; j < area[0].length; j++) {
                        resultStep[i][j] = LAND;
                    }
                }
                for (int i = 0; i < area.length; i++) {
                    for (int j = 0; j < area[0].length; j++) {
                        if (area[i][j] == WATER) {
                            resultStep[i][j] = WATER;
                            if (i < area.length - 1) {
                                resultStep[i + 1][j] = WATER;
                            }
                            if (j < area[0].length - 1) {
                                resultStep[i][j + 1] = WATER;
                            }
                            if (i > 0) {
                                resultStep[i - 1][j] = WATER;
                            }
                            if (j > 0) {
                                resultStep[i][j - 1] = WATER;
                            }
                        }
                    }
                }

                if (Arrays.deepEquals(resultStep, area)) {
                    return;
                }
                flood(newArea(resultStep), logger);
            }

            private String newArea(char[][] area) {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < area.length; i++) {
                    for (int j = 0; j < area[0].length; j++) {
                        builder.append(area[i][j]);
                    }
                    builder.append("\n");
                }
                builder.deleteCharAt(builder.length() - 1);
                return builder.toString();
            }
        };

        return floodFill;
    }

    char LAND = '█';
    char WATER = '░';
}
