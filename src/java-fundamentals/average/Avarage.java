package java_fundamentals.average;

import java.util.Scanner;

class Average {
        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
            int count = 0;
            int n = 0;
            int sum = 0;
            int result = 0;
            System.out.println("Введите число: ");
            while (true) {
                n = sc.nextInt();
                if (n == 0) {
                    break;
                }
                count++;
                sum += n;
                result = sum / count;
            }
            System.out.println(result);
        }
    }
