package java_fundamentals.electronicwatch;

import java.util.Scanner;

public class ElectronicWatch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int seconds = scanner.nextInt();
        int h = seconds / 3600 % 24;
        int m = seconds / 60 % 60;
        int s = seconds % 60;
        System.out.println(h + ":" + ElectronicWatch.addZero(m) + ":" + ElectronicWatch.addZero(s));
    }

    public static String addZero(int n) {
        if (n <= 9) {
            return "0" + n;
        } else {
            return n + "";
        }
    }
}
