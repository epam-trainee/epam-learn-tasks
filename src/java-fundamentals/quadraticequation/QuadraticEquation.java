package java_fundamentals.quadraticequation;

import java.util.Scanner;

public class QuadraticEquation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        double c = scanner.nextDouble();

        double d = b * b - 4 * a * c;
        double x1 = (-b - Math.sqrt(d)) / (2 * a);
        double x2 = (-b + Math.sqrt(d)) / (2 * a);
        if (d == 0)
            System.out.printf(String.valueOf(x1));
        else if (d < 0)
            System.out.print("no roots");
        else if (x1 < x2)
            System.out.print(x1 + " " + x2);
        else
            System.out.print(x2 + " " + x1);

    }
}

