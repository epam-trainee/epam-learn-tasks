package testquadraticequation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class QuadraticEquationTwoRootsCasesTesting {
    protected QuadraticEquation quadraticEquation = new QuadraticEquation();

    private double a;
    private double b;
    private double c;
    private String expected;

    @Parameterized.Parameters
    public static Iterable<Object[]> dataForTest () {
        return Arrays.asList(new Object[][]{
                {1.0, 2.0, -3.0, "1.0 -3.0"},
                {1.0, -3.0, -4.0, "4.0 -1.0"},
                {3.0, -33.0, 90.0, "6.0 5.0"},
                {1.0, -26.0, 120.0, "20.0 6.0"},
                {2.0, 5.0, -3, "-3.0 0.5"}
        });
    }

    public QuadraticEquationTwoRootsCasesTesting (double a, double b, double c, String expected) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.expected = expected;
    }

    @Test
    public void testTwoRootsCase () {
        String[] arrOfExpStrings = expected.split(" ");
        List<Double> expNumbers
                = Arrays.stream(arrOfExpStrings)
                .map(Double::valueOf)
                .collect(Collectors.toList());
        String result = quadraticEquation.solve(a, b, c);
        if (result.equals("no roots")) {
            throw new AssertionError();
        }
        String[] arrOfResStrings = result.split(" ");
        List<Double> resNumbers
                = Arrays.stream(arrOfResStrings)
                .map(Double::valueOf)
                .collect(Collectors.toList());
        assertTrue(expNumbers.containsAll(resNumbers));
    }

}
