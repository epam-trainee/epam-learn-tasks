package testquadraticequation;

import static java.lang.Math.sqrt;

public class QuadraticEquation {
    public String solve(double a, double b, double c) {
        if (a == 0) {
            throw new IllegalArgumentException();
        }
        double d = (b * b) - (4 * a * c);
        if (d < 0) {
            return "no roots";}
        else if (d == 0) {
            return String.valueOf(-b / (2 * a));
        }
        else {
            double x1=(-b+sqrt(d))/(2*a);
            double x2=(-b-sqrt(d))/(2*a);
            return x1+" "+x2;
        }
    }
}