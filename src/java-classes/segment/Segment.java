package java_classes.segment;

import java.util.Objects;

class Segment {

    private Point start;
    private Point end;


    public Segment(Point start, Point end)  {

        try {
            if (start.equals(end) ||  end == null || start == end ||  end == null) {
                throw new RuntimeException("Start and end of segment should be differ!");
            }

        } catch (RuntimeException e) {
            e.printStackTrace();
            {
                System.out.println(e.getMessage());
//            System.err.println("String can not be empty!" + e.getMessage());
            }
        }
        try {
            if (start.equals(end) || end == null || start == end ||  end == null) {
                throw new RuntimeException("Start and end of segment should be differ!");
            }
        } catch (RuntimeException re) {
            re.printStackTrace();
            throw new RuntimeException();
        }
        this.start = start;
        this.end = end;
    }


    double length() {
        double xDistanceSquare = Math.pow(start.getX() - end.getX(), 2);
        double yDistanceSquare = Math.pow(start.getY() - end.getY(), 2);
        return Math.sqrt(xDistanceSquare + yDistanceSquare);
    }

    Point middle() {
        double midX = (((start.getX()) + (end.getX())) / 2);
        double midY = (((start.getY()) + (end.getY())) / 2);
        return new Point((midX), (midY));
    }

    Point intersection(Segment another) {
        double D = (start.getX() - end.getX()) * (another.start.getY() - another.end.getY()) - (start.getY() - end.getY()) * (another.start.getX() - another.end.getX());
        double Px = ((start.getX() * end.getY() - start.getY() * end.getX()) * (another.start.getX() - another.end.getX()) - (start.getX() - end.getX()) * (another.start.getX() * another.end.getY() - another.start.getY() * another.end.getX())) / D;
        double Py = ((start.getX() * end.getY() - start.getY() * end.getX()) * (another.start.getY() - another.end.getY()) - (start.getY() - end.getY()) * (another.start.getX() * another.end.getY() - another.start.getY() * another.end.getX())) / D;
        boolean a = Math.min(start.getX(), end.getX()) <= Px && Px <= Math.max(start.getX(), end.getX());
        boolean b = Math.min(another.start.getX(), another.end.getX()) <= Px && Px <= Math.max(another.start.getX(), another.end.getX());

        if (D == 0 || !a || !b) {
            return null;
        } else {
            return new Point(Px, Py);
        }
    }

    public Point getStart() {
        return start;
    }

    public void setStart(Point start) {
        this.start = start;
    }

    public Point getEnd() {
        return end;
    }

    public void setEnd(Point end) {
        this.end = end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Segment segment = (Segment) o;
        return Objects.equals(start, segment.start) && Objects.equals(end, segment.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }

    @Override
    public String toString() {
        return "Segment{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }
}
