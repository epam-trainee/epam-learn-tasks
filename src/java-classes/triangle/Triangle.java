package java_classes.triangle;

public class Triangle {
    private Point a, b, c;

    // length of the 3 sides
    double sideAB;
    double sideBC;
    double sideAC;

    public Triangle(double sideAB, double sideBC, double sideAC) {
        this.sideAB = sideAB;
        this.sideBC = sideBC;
        this.sideAC = sideAC;
    }

    public Triangle() {
    }

    // Constructor : Takes 3 vertices, calculates sides
    public Triangle(Point a, Point b, Point c) {
        this.a = a;
        this.b = b;
        this.c = c;
        sideAB = a.DistanceBetween(b);
        sideBC = b.DistanceBetween(c);
        sideAC = a.DistanceBetween(c);
        if (sideAB > 0 && sideBC > 0 && sideAC > 0)
            if (((sideAB + sideBC) >= sideAC) && ((sideAB + sideAC) >= sideBC) && ((sideAC + sideBC) >= sideAB)) {
                this.a = a;
                this.b = b;
                this.c = c;
            } else throw new RuntimeException();
        if (area() == 0) {
            throw new RuntimeException();
        }
    }

    public Triangle(double sideAB, double sideBC, double sideAC, Point a, Point b, Point c) {
        if (((sideAB + sideBC) >= sideAC) && ((sideAB + sideAC) >= sideBC) && ((sideAC + sideBC) >= sideAB)) {
            this.a = a;
            this.b = b;
            this.c = c;
        } else throw new RuntimeException();
        if (area() == 0) {
            throw new RuntimeException();
        }
    }

    public double area() {
        double s = (sideAB + sideBC + sideAC) / 2;
        return Math.sqrt(s * (s - sideAB) * (s - sideBC) * (s - sideAC));

    }

    public Point centroid() {
        return new Point((a.getX() + b.getX() + c.getX()) / 3,
                (a.getY() + b.getY() + c.getY()) / 3);
    }
}