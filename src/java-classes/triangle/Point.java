package java_classes.triangle;

class Point {
    private final double x;
    private final double y;

    public Point(final double x, final double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double DistanceBetween(Point point) {
        double distance, xd, yd;
        xd = x - point.getX();
        yd = y - point.getY();
        distance = Math.sqrt(xd * xd + yd * yd);

        return distance;
    }
}
