package java_classes.figures;

class Point {
    private double x;
    private double y;

    public Point(final double x, final double y) {
        this.x = x;
        this.y = y;
    }

    public Point(double x, double y, double x1, double y1, double x2, double y2) {
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double DistanceBetween(Point point) {
        double distance, xd, yd;
        xd = x - point.getX();
        yd = y - point.getY();
        distance = Math.sqrt(xd * xd + yd * yd);
        return distance;
    }


    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
