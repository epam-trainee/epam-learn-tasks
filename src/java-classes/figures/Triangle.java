package java_classes.figures;

import java.util.Locale;

class Triangle extends Figure {
    Point a;
    Point b;
    Point c;

    public Triangle(Point a, Point b, Point c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double area() {
        return Math.abs((a.getX() - c.getX()) * (b.getY() - a.getY()) -
                (a.getX() - b.getX()) * (c.getY() - a.getY())) * 0.5;
    }

    @Override
    public String pointsToString() {
        return String.format(Locale.ROOT, "(%.1f,%.1f)(%.1f,%.1f)(%.1f,%.1f)",
                a.getX(), a.getY(), b.getX(), b.getY(), c.getX(), c.getY());
    }

    @Override
    public String toString() {
        return String.format(Locale.ROOT, "Triangle[(%.1f,%.1f)(%.1f,%.1f)(%.1f,%.1f)]",
                a.getX(), a.getY(), b.getX(), b.getY(), c.getX(), c.getY());
    }

    @Override
    public Point leftmostPoint() {
        Point[] points = {a, b, c};
        Point left = a;
        for (Point point : points) {
            if (point.getX() < left.getX()) {
                left = point;
            }
        }
        return left;
    }
}
