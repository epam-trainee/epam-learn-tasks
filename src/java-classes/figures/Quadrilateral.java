package java_classes.figures;

import java.util.Locale;

class Quadrilateral extends Figure {
    Point a;
    Point b;
    Point c;
    Point d;

    public Quadrilateral(Point a, Point b, Point c, Point d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    @Override
    public double area() {
        Triangle tr1 = new Triangle(a, b, c);
        Triangle tr2 = new Triangle(a, d, c);
        return tr1.area() + tr2.area();
    }

    @Override
    public String pointsToString() {
        //(a.x,a.y)(b.x,b.y)(c.x,c.y)(d.x, d.y)
        return String.format(Locale.ROOT, "(%.1f,%.1f)(%.1f,%.1f)(%.1f,%.1f)(%.1f,%.1f)",
                a.getX(), a.getY(), b.getX(), b.getY(), c.getX(), c.getY(), d.getX(), d.getY());
    }

    @Override
    public String toString() {
        //Quadrilateral[(a.x,a.y)(b.x,b.y)(c.x,c.y)(d.x, d.y)]
        return String.format(Locale.ROOT, "Quadrilateral[(%.1f,%.1f)(%.1f,%.1f)(%.1f,%.1f)(%.1f,%.1f)]",
                a.getX(), a.getY(), b.getX(), b.getY(), c.getX(), c.getY(), d.getX(), d.getY());
    }

    @Override
    public Point leftmostPoint() {
        Point[] points = {a, b, c, d};
        Point left = a;
        for (Point point : points) {
            if (point.getX() < left.getX()) {
                left = point;
            }
        }
        return left;
    }
}